/*---------------------------------------  info  -------------------------------------------------*/
		//implementation of an associative array in C. 
		//written by: github/return5
		//license: GPL 2.0
		//i make no claim this is the best or most effecient way to accomplish associative arrays in C

/*---------------------------------------  header files  -----------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*---------------------------------------  typedefs ----------------------------------------------*/
typedef struct associativeArray{ //struct which is used to create an associative array. this one is limited to int as values
	int *values;  //array to hold values
	char **keys;  //array to hold keys
	size_t size_value,size_key;  //size of elements in value array and size of elements in keys array
	int index,length;  //index for the arrays and the length of the arrays
}associativeArray;

/*--------------------------------------- prototypes ---------------------------------------------*/
void makeAssArray(associativeArray *const assarr); 
void addToArray(associativeArray *const assarr, const char *const key, const int value); 
void checkSize(associativeArray *const assarr); 
void makeKey(associativeArray *const assarr, const char *const new_key); 
void makeKey(associativeArray *const assarr, const char *const new_key); 
int getElement(const associativeArray *const assarr, const char *const key); 
int getKeyIndex(const associativeArray *const assarr,const char *const key); 

/*---------------------------------------  code --------------------------------------------------*/

//gets index for that key, if key exists, otherwise return -1
int getKeyIndex(const associativeArray *const assarr,const char *const key) {
	for(int i = 0; i < assarr->index; i++ ) {
		//checks which string is shorter and uses that length
		const size_t len = (strlen(assarr->keys[i]) < strlen(key)) ? strlen(assarr->keys[i]) : strlen(key);
		//if key exists, then return its index
		if(strncmp(assarr->keys[i],key,len) == 0) {
			return i;
		}
	}
	//key doesnt exist, so return -1
	return -1;
}

//returns element in values array which corresponds to key if key exists, otherwise return -9999
int getElement(const associativeArray *const assarr, const char *const key) {
	const int key_index = getKeyIndex(assarr,key);
	return (key_index != -1) ? assarr->values[key_index] : -9999;
}

//adds new key to keys array
void makeKey(associativeArray *const assarr, const char *const new_key) {
	assarr->keys[assarr->index] = malloc(strlen(new_key)+1);
	snprintf(assarr->keys[assarr->index],sizeof(assarr->keys[assarr->index]),new_key);
}

//checks if key already exists, and if so, replaces old value with the new value
int checkKeyAlreadyExist(associativeArray *const assarr, const char *const key, const int new_value) {
	const int index = getKeyIndex(assarr,key);
	if(index != -1) {		
		assarr->values[index] = new_value;
		return 0;
	}
	return 1;
}
//checks size of arrays to see if they need to be allocated more space.
void checkSize(associativeArray *const assarr) {
	if(assarr->index == assarr->length) {
		assarr->length += 5;
		int *temp_value = realloc(assarr->values,assarr->size_value * assarr->length);
		char **temp_keys = realloc(assarr->keys,assarr->size_key * assarr->length);
		if(temp_value == NULL) {
 			temp_value = realloc(assarr->values,assarr->size_value * assarr->length);
		}
		if(temp_keys == NULL) {
 			temp_keys = realloc(assarr->keys,assarr->size_key * assarr->length);
		}
		assarr->values = temp_value;
		assarr->keys = temp_keys;
	}
}

//add an item to array.
void addToArray(associativeArray *const assarr, const char *const key, const int value) {
	checkSize(assarr);
	if (checkKeyAlreadyExist(assarr,key,value) != 0) {
		makeKey(assarr,key);
		assarr->values[(assarr->index)++] = value;
	}
}


//makes an associatve array object from an associativeArray struct
void makeAssArray(associativeArray *const assarr) {
	assarr->size_value = sizeof(int);
	assarr->size_key = sizeof(char*);
	assarr->length = 5;
	assarr->values = malloc(assarr->size_value * assarr->length);	
	assarr->keys = malloc(assarr->size_key * assarr->length);
	assarr->index = 0;
}

int main(void) {
	associativeArray assarr;
	makeAssArray(&assarr);
	addToArray(&assarr,"hello",5);  //key is 'hello' and value is 5
	addToArray(&assarr,"world",4);
	addToArray(&assarr,"how",6);
	addToArray(&assarr,"how",10);  //replace 6 with 10 for key 'how'
	addToArray(&assarr,"are",7);
	addToArray(&assarr,"you",8);
	printf("hello is %d\n",getElement(&assarr,"hello"));  //should return 5
	printf("world is %d\n",getElement(&assarr,"world"));
	printf("how is %d\n",getElement(&assarr,"how"));  //should return 10
	printf("are is %d\n",getElement(&assarr,"are"));
	printf("you is %d\n",getElement(&assarr,"you"));
	printf("fine is %d\n",getElement(&assarr,"fine")); //should return -9999 since fine isnt a valid key
	return 0;
}
